﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace project.Models
{
    [MetadataType(typeof(UsersMetadata))]
    public partial class Users
    {
    }

    public class UsersMetadata {
        [Required(ErrorMessage = "必填")]
        [DisplayName("姓名")]
        [StringLength(10)]
        public string Name { get; set; }

        [Required(ErrorMessage = "必填")]
        [DisplayName("Email")]
        [StringLength(50)]
        [EmailAddress]
        public string Email { get; set; }

        [Required(ErrorMessage = "必填")]
        [DisplayName("密碼")]
        [StringLength(10)]
        public string Password { get; set; }

        [Required(ErrorMessage = "必填")]
        [DisplayName("生日")]
        public System.DateTime Birthday { get; set; }

        [Required(ErrorMessage = "必填")]
        [DisplayName("性別")]
        public bool Gender { get; set; }
    }
}