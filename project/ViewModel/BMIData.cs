﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace project.ViewModel
{
    public class BMIData
    {
        public float Weight { get; set; }
        public float Height { get; set; }
        public float BMI { get; set; }
        public string Level { get; set; }
    }
}